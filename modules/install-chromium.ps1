
$current_dir=$PWD.Path

$home_path="C:\Users\user"
$base_path="$current_dir\.."
$base_dir_name="windows-shell-scripts"

if ((get-item $base_path).Name -ne $base_dir_name) {
  $base_path="$current_dir"
}

$update_chromium_path="$home_path\update_chromium.ps1"
$update_chromium_copy="$base_path\user-settings\update_chromium.ps1"

Start-Sleep -Seconds 1.5
Write-Output "Now checking if Chromium is installed...`n"
Start-Sleep -Seconds 1.5

$hasChromium = (winget list --id eloston.ungoogled-chromium --source winget --accept-source-agreements) -join '';
$regExNotInstalled = '^.*No installed package found matching input criteria\..*$';

if ($hasChromium -notmatch $regExNotInstalled) {

  Write-Output "> Chromium is already installed!`n"

} else {

  if (Get-Command winget) {

    Write-Output "Chromium does not exist!`n"
    Start-Sleep -Seconds 1.5
    Write-Output "> Installing Chromium now...`n"
    winget install -e eloston.ungoogled-chromium
    Start-Sleep -Seconds 1.5
    Write-Output "Chromium is now installed!"

    Start-Sleep -Seconds 1.5
    Write-Output "Checking to see if 'update_chromium.ps1' script exists...`n"
    Start-Sleep -Seconds 1.5
    
    if (Test-Path $update_chromium_path) {

      Write-Output "> 'update_chromium.ps1' script is already installed!`n"

    } else {

      Write-Output "> 'update_chromium.ps1' script was not found!"
      Start-Sleep -Seconds 1.5
      Write-Output "> Copying file now..."
      Copy-Item -Path $update_chromium_copy -Destination $home_path
      Start-Sleep -Seconds 1.5
      Write-Output "> Done!`n"
      
      Start-Sleep -Seconds 1.5
      Write-Output "Now creating a task to check for Chromium update at logon..."

      $taskUser="$env:USERDOMAIN\$env:USERNAME"
      $scriptUpdateChromiumPath='C:\Users\user\update_chromium.ps1'
      $taskTrigger=New-ScheduledTaskTrigger -AtLogon
      $taskAction=New-ScheduledTaskAction -Execute "Powershell.exe" -Argument "-File $scriptUpdateChromiumPath"
      $taskSettings=New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -ExecutionTimeLimit 0

      Register-ScheduledTask -TaskName "Update_Chromium" -Trigger $taskTrigger -User $taskUser -Action $taskAction -Settings $taskSettings -RunLevel Highest -Force

      Start-Sleep -Seconds 1.5
      Write-Output "Done!"

    }

  } else {

    Write-Output "winget does not exist!"
    Start-Sleep -Seconds 1.5
    Write-Output "Chromium was not installed!"

  }

}
