@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set HOMEPATH="C:\Users\user"

set GIT_PROMPT_DIR_PATH=%HOMEPATH%\.config\git\
set GIT_PROMPT_DIR_COPY=%BASEDIR%user-settings\.config\git

set BASH_PROFILE_PATH=%HOMEPATH%\.bash_profile
set BASH_PROFILE_COPY=%BASEDIR%user-settings\.bash_profile

set GIT_CONFIG_FILE_PATH=%HOMEPATH%\.gitconfig
set GIT_CONFIG_FILE_COPY=%BASEDIR%user-settings\.gitconfig

set GIT_IGNORE_PATH=%HOMEPATH%\.gitignore_global
set GIT_IGNORE_COPY=%BASEDIR%user-settings\.gitignore_global

set MINTTY_PATH=%HOMEPATH%\.minttyrc
set MINTTY_COPY=%BASEDIR%user-settings\.minttyrc

call:%~1
goto:eof

:func
  echo:
  echo Copying Git Bash settings now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if Git prompt settings exists...
  if not exist %GIT_PROMPT_DIR_PATH% (
    timeout /t 2 /nobreak > nul
    echo ^> Git prompt settings were not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying Git prompt settings into %GIT_PROMPT_DIR_PATH%
    echo:
    timeout /t 2 /nobreak > nul
    xcopy %GIT_PROMPT_DIR_COPY% %GIT_PROMPT_DIR_PATH% /s /f
    timeout /t 2 /nobreak > nul
    echo:
    echo DONE!
    echo:
  ) else (
    echo ^> Git prompt settings already exist!
    timeout /t 2 /nobreak > nul
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if '%BASH_PROFILE_PATH%' file exists...
  if not exist %BASH_PROFILE_PATH% (
    timeout /t 2 /nobreak > nul
    echo '^> %BASH_PROFILE_PATH%' was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying bash profile into '%HOMEPATH%'
    echo:
    timeout /t 2 /nobreak > nul
    copy /Y %BASH_PROFILE_COPY% %HOMEPATH%
    timeout /t 2 /nobreak > nul
    echo DONE!
    echo:
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> Bash profile is already installed!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if git config settings exists...
  if not exist %GIT_CONFIG_FILE_PATH% (
    timeout /t 2 /nobreak > nul
    echo '^> %GIT_CONFIG_FILE_PATH%' was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying git config settings into '%HOMEPATH%'
    echo:
    timeout /t 2 /nobreak > nul
    copy /Y %GIT_CONFIG_FILE_COPY% %HOMEPATH%
    timeout /t 2 /nobreak > nul
    echo DONE!
    echo:
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> Git config settings are already installed!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if git ignore settings exists...
  if not exist %GIT_IGNORE_PATH% (
    timeout /t 2 /nobreak > nul
    echo '^> %GIT_IGNORE_PATH%' was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying git config settings into '%HOMEPATH%'
    echo:
    timeout /t 2 /nobreak > nul
    copy /Y %GIT_IGNORE_COPY% %HOMEPATH%
    timeout /t 2 /nobreak > nul
    echo DONE!
    echo:
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> Git ignore settings are already installed!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if git bash customizations exist...
  if not exist %MINTTY_PATH% (
    timeout /t 2 /nobreak > nul
    echo '^> %MINTTY_PATH%' was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying git bash customizations into '%HOMEPATH%'
    echo:
    timeout /t 2 /nobreak > nul
    copy /Y %MINTTY_COPY% %HOMEPATH%
    timeout /t 2 /nobreak > nul
    echo DONE!
    echo:
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> Git bash customizations are already installed!
  )
goto:eof