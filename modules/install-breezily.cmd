@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set PROGRAM_FILES_PATH_PS='%ROOTDIR%Program Files\'
set BREEZILY_COPY=%BASEDIR%portable-apps\Breezily-Portable\
set BREEZILY_PATH="%ROOTDIR%Program Files\"Breezily-Portable\

call:%~1
goto:eof

:func
  echo:
  echo Installing Breezily program now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if Breezily already exists...

   if not exist %BREEZILY_PATH% (
    timeout /t 2 /nobreak > nul
    echo ^> Breezily program was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying Breezily into %BREEZILY_PATH%
    echo:
    timeout /t 2 /nobreak > nul
    xcopy %BREEZILY_COPY% %BREEZILY_PATH% /s /f
    timeout /t 2 /nobreak > nul
    echo:
    echo DONE!
    echo:
    timeout /t 2 /nobreak > nul
    echo Creating a Breezily task to execute program at logon...
    timeout /t 2 /nobreak > nul
    powershell $user= 'user'; $breezilyPath= 'C:\Program Files\Breezily-Portable\breezily.exe'; $trigger= "New-ScheduledTaskTrigger -AtLogon"; $action= New-ScheduledTaskAction -Execute $breezilyPath; $settings= New-ScheduledTaskSettingsSet  -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -ExecutionTimeLimit 0; Register-ScheduledTask -TaskName "Start_Breezily" -Trigger $trigger -User $user -Action $action -Settings $settings -RunLevel Highest -Force
    timeout /t 2 /nobreak > nul
    echo DONE!
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> Breezily is already installed!
  )
goto:eof
