@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set HOMEPATH="C:\Users\user"

set GIT_BASH_COPY=%BASEDIR%portable-apps\GitForWindows-Portable.7z
set GIT_BASH_PATH="%ROOTDIR%Program Files"\GitForWindows-Portable\
set GIT_BASH_PATH_PS='%ROOTDIR%Program Files\GitForWindows-Portable\'

set GIT_BASH_START_MENU_PATH="%HOMEPATH%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\git-bash.exe"
set GIT_BASH_EXE="C:\Program Files\"GitForWindows-Portable\git-bash.exe

call:%~1
goto:eof

:func
  echo:
  echo Installing Git Bash portable now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if Git Bash Portable exists...
  if not exist %GIT_BASH_PATH% (
    timeout /t 2 /nobreak > nul
    echo Git Bash Portable was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying Git Bash Portable into %GIT_BASH_PATH%
    echo:
    timeout /t 2 /nobreak > nul
    mkdir %GIT_BASH_PATH%
    powershell 7z x %GIT_BASH_COPY% -o%GIT_BASH_PATH_PS% -r
    timeout /t 2 /nobreak > nul
    echo DONE!
    echo:
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> Git Bash portable is already installed!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Now adding Git Bash to the start menu...
  timeout /t 2 /nobreak > nul
  if not exist %GIT_BASH_START_MENU_PATH% (
    mklink %GIT_BASH_START_MENU_PATH% %GIT_BASH_EXE%
  ) else (
    echo ^> Git Bash is already in the start menu!
  )
goto:eof