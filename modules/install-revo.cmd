@echo off

pushd \
set ROOTDIR=%CD%
popd

set BASEDIR=%~dp0..\
set PROGRAM_FILES_PATH_PS='%ROOTDIR%Program Files\'
set REVO_COPY=%BASEDIR%portable-apps\RevoUninstaller-Portable.zip
set REVO_PATH="%ROOTDIR%Program Files\"RevoUninstaller-Portable\

set REVO_START_MENU_PATH="%HOMEPATH%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\RevoUPort.exe"
set REVO_EXE="C:\Program Files\"RevoUninstaller-Portable\RevoUPort.exe

call:%~1
goto:eof

:func
  echo:
  echo Installing RevoUninstaller program now...
  echo:
  timeout /t 2 /nobreak > nul
  echo Checking if RevoUninstaller already exists...
   if not exist %REVO_PATH% (
    timeout /t 2 /nobreak > nul
    echo ^> RevoUninstaller was not found!
    timeout /t 2 /nobreak > nul
    echo ^> Copying RevoUninstaller into %REVO_PATH%
    echo:
    timeout /t 2 /nobreak > nul
    powershell Expand-Archive %REVO_COPY% -DestinationPath %PROGRAM_FILES_PATH_PS%
    timeout /t 2 /nobreak > nul
    echo DONE!
  ) else (
    timeout /t 2 /nobreak > nul
    echo ^> RevoUninstaller is already installed!
  )
  echo:
  timeout /t 2 /nobreak > nul
  echo Now adding RevoUninstaller to the start menu...
  timeout /t 2 /nobreak > nul
  if not exist %REVO_START_MENU_PATH% (
    mklink %REVO_START_MENU_PATH% %REVO_EXE%
  ) else (
    echo ^> RevoUninstaller is already in the start menu!
  )
goto:eof