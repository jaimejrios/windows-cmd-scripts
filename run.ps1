$currentDir=$PWD.Path

$scriptList = @(
    "$currentDir\modules\install-scoop.ps1"
    "$currentDir\modules\install-git.ps1"
    "$currentDir\modules\install-codium.ps1"
    "$currentDir\modules\copy-codium-settings.ps1"
);

Write-Output "`nWelcome to your setup script!"
Start-Sleep -Seconds 1.5
$InputFromUser = Read-Host -Prompt "`n> Do you want to continue? (Y|n)"

if ($InputFromUser -eq "Y") {
  foreach ($script in $scriptList) {
    & $script
  }
  Start-Sleep -Seconds 1.5
  Write-Output "The powershell script has finished!`n"
  Start-Sleep -Seconds 1.5
  Write-Output "Goodbye...`n"

} elseif ($InputFromUser -eq "N") {
  Start-Sleep -Seconds 1.5
  Write-Output "Now exiting the powershell script.`n"
  Start-Sleep -Seconds 1.5
  Write-Output "Goodbye...`n"
}
